﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class EatFood : MonoBehaviour
{
    public UnityEvent Eaten;

    // Update is called once per frame
    void Update( ) { }

    void OnCollisionEnter( Collision col )
    {
        if ( col.gameObject.tag == "Player" )
        {
            Objectives.Singleton.FoodEatens();
            AudioSource.PlayClipAtPoint(Assets.Singleton.Eat, transform.position, 0.4f);
            Eaten.Invoke( );
            Destroy( gameObject );
        }
    }
}
