﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class noose : MonoBehaviour
{
    public GameObject noosePrefab;
    public Transform spawnpoint;
    public Transform spawnedNoose;

    private Vector3 hitloc;
    private ConfigurableJoint cj;
    private LineRenderer lr;
    private bool on;


    /// <summary>
    /// Called immediately after this <see cref="Component"/> instance is created.
    /// This is essentially a constructor for a <see cref="Component"/>, you cannot define your own constructor.
    /// </summary>
    private void Awake( )
    {
        cj = GetComponent< ConfigurableJoint >( );
        spawnedNoose = null;
        lr = GetComponent< LineRenderer >( );
        lr.enabled = false;
    }

    void OnCollisionEnter( Collision col )
    {
        if ( col.gameObject.tag == "Dart" && spawnedNoose == null)
        {
            spawnedNoose = Instantiate( noosePrefab, spawnpoint.position, spawnpoint.rotation ).transform;
            cj.connectedBody = spawnedNoose.GetComponent< Rigidbody>( );
            hitloc = col.transform.position;
            cj.anchor = transform.InverseTransformPoint(hitloc);
        }
    }

     /// <summary>
    /// Called each render frame and allows the <see cref="Component"/> to continually update its internal state while enabled.
    /// </summary>
    private void Update( )
    {
        if ( spawnedNoose != null && ( spawnedNoose.hasChanged || transform.hasChanged ) )
        {
            lr.enabled = true;
            Vector3[] vec = { hitloc, spawnedNoose.position};
            lr.SetPositions(vec);
        }
    }
}
