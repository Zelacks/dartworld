﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hang : MonoBehaviour
{
    public GameObject hangSpawn;
	// Use this for initialization
	void Start () {
		
	}

    void OnTriggerEnter( Collider col )
    {
        if ( col.gameObject.tag == "Player" )
        {
            var person = FindObjectOfType< FirstPerson >( ).InControl = false;
            col.attachedRigidbody.velocity = Vector3.zero;
            GetComponent< Rigidbody >( ).isKinematic = true;
            GetComponent< Rigidbody >( ).useGravity = false;
            Instantiate(hangSpawn);
        }
    }
}
