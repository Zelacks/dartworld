﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Objectives : MonoBehaviour
{
    public bool FoodEaten;
    public Text ObjectiveText;
    private ObjectiveState state;
    public Transform Rat;

    public ObjectiveState State
    {
        get { return state; }
    }

    private static Objectives singleton;

    public static Objectives Singleton
    {
        get
        {
            if (singleton == null)
            {
                singleton = FindObjectOfType<Objectives>();
            }
            return singleton;
        }
    }


    public enum ObjectiveState
    {
        EatFood,
        CheckFriend,
        KillRat,
        CheckGF,
        KillSelf
    }

    void Awake()
    {
        state = ObjectiveState.EatFood;
        CheckObjectives();
    }

    public void CheckObjectives()
    {
        switch (state)
        {
            case ObjectiveState.EatFood:
                ObjectiveText.text = "Find food to eat.";
                break;
            case ObjectiveState.CheckFriend:
                ObjectiveText.text = "Turn on and check computer";
                break;
            case ObjectiveState.KillRat:
                ObjectiveText.text = "Kill the rat";
                break;
            case ObjectiveState.CheckGF:
                ObjectiveText.text = "Respond to computer";
                break;
            case ObjectiveState.KillSelf:
                ObjectiveText.text = "???";
                break;
        }
    }

    public void FoodEatens()
    {
        if (state == ObjectiveState.EatFood)
        {
            state = ObjectiveState.CheckFriend;
        }


        CheckObjectives();
    }

    public void ChatBuddo()
    {
        if (state == ObjectiveState.CheckFriend)
        {
            state = ObjectiveState.KillRat;
            Rat.gameObject.SetActive(true);
        }
        CheckObjectives();
    }

    public void KilledRat()
    {
        state = ObjectiveState.CheckGF;
        CheckObjectives();
    }

    public void QuestionMark()
    {
        state = ObjectiveState.KillSelf;
        CheckObjectives();
    }

}