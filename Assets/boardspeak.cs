﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class boardspeak : MonoBehaviour
{
    private AudioSource ass;

    void Awake()
    {
        ass = GetComponent<AudioSource>();
    }


    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag.Equals("Dart"))
        {
            Debug.Log(Vector3.Distance(transform.position, col.transform.position));

            if (Vector3.Distance(transform.position, col.transform.position) < 0.115f)
            {
                ass.clip = Assets.Singleton.Bullseye.OrderBy(qu => Guid.NewGuid()).First();
            }
            else
            {
                ass.clip = Assets.Singleton.BoardResponses.OrderBy(qu => Guid.NewGuid()).First();
            }

            ass.Play();
        }
    }
}