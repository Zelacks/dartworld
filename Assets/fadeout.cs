﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class fadeout : MonoBehaviour
{
    public float fadeouttime = 5.0f;
    private float curtime;
    private Image img;
    /// <summary>
    /// Called immediately after this <see cref="Component"/> instance is created.
    /// This is essentially a constructor for a <see cref="Component"/>, you cannot define your own constructor.
    /// </summary>
    private void Awake( )
    {
        fadeouttime = 5.0f;
        transform.SetParent( FindObjectOfType< Canvas >( ).transform );
        RectTransform yeah = (RectTransform )transform;
        yeah.anchorMin = Vector2.zero;
        yeah.anchorMax = Vector2.one;
        yeah.offsetMin = Vector2.zero;
        yeah.offsetMax = Vector2.zero;
        img = GetComponent< Image >( );
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
	    curtime += Time.deltaTime;
	    img.color = Color.Lerp( Color.clear, Color.black, curtime / fadeouttime );
	}
}
