﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour {

	
	// Update is called once per frame
	void Update ()
	{
	    transform.localRotation = transform.localRotation * Quaternion.AngleAxis(360 * Time.deltaTime, Vector3.forward);
	}
}
