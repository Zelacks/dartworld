﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking.NetworkSystem;


public class DartHand : MonoBehaviour
{
    public GameObject DartPrefab;
    public GameObject DartAnchor;
    public int ThrowButton = 0;
    public float MinThrowStrength = 10f;
    public float MaxThrowStrength = 300f;
    public float SecondsToCharge = 2f;
    public float SecondsToThrowMin = 0.2f;
    public float SecondsToThrowMax = 0.1f;
    public float SecondsToReload = 1f;
    public GameObject Hand;
    public GameObject ChargedPos;
    public GameObject IdlePos;
    public GameObject ReloadPos;
    public GameObject ThrowPos;
    public Vector3 storedHandPosOffset;
    public Quaternion storedHandRotOffset;
    public Transform HandPos;
    public FirstPerson person;

    private GameObject heldDart;
    private float chargeProgress = 0f; // 0 to 1
    private float throwProgress = 0f; // 0 to 1
    private float reloadProgress = 0f; // 0 to 1
    private float stickyProfress = 0f;
    private State curState = State.Idle;

    public bool sticky = false;
    private ConfigurableJoint cj;
    private Transform attachedDart;
    private LineRenderer lr;



    private enum State
    {
        Idle,
        Charging,
        Throw,
        Reload,
        Sticky
    }

    /// <summary>
    /// Called immediately after this <see cref="Component"/> instance is created.
    /// This is essentially a constructor for a <see cref="Component"/>, you cannot define your own constructor.
    /// </summary>
    private void Awake( )
    {
        changeState( State.Reload );
        cj = GetComponent< ConfigurableJoint >( );
        lr = GetComponent< LineRenderer >( );
        lr.enabled = false;
    }


    // Use this for initialization
    void Start( ) { }

    // Update is called once per frame
    void Update( )
    {
        switch ( curState )
        {
            case State.Charging:
                chargeProgress += Time.deltaTime / SecondsToCharge;
                chargeProgress = Mathf.Clamp( chargeProgress, 0, 1 );

                //Debug.Log( chargeProgress );

                moveHand( ChargedPos.transform, chargeProgress );

                if ( Input.GetMouseButtonUp( ThrowButton ) && person.InControl )
                {
                    changeState( State.Throw );
                }
                break;
            case State.Throw:
                throwProgress += Time.deltaTime / Mathf.Lerp( SecondsToThrowMin, SecondsToThrowMax, chargeProgress );

                moveHand( ThrowPos.transform, throwProgress );

                if ( throwProgress > 0.5f && heldDart != null )
                {
                    throwDart( );
                }

                if ( throwProgress > 0.999 )
                {
                    throwProgress = 0;
                    if ( !sticky )
                    {
                        changeState( State.Reload );
                    }
                    else
                    {
                        changeState(State.Sticky);
                    }
                }

                break;
            case State.Reload:
                reloadProgress += Time.deltaTime / SecondsToReload;
                reloadProgress = Mathf.Clamp( reloadProgress, 0, 1 );

                if ( reloadProgress < 0.5f )
                {
                    moveHand( ReloadPos.transform, Mathf.Clamp( reloadProgress * 2, 0f, 1f ) );
                }
                else
                {
                    if ( heldDart == null )
                    {
                        updateStoredHandTransform( );
                        spawnDart( );
                    }

                    moveHand( IdlePos.transform, Mathf.Clamp( reloadProgress * 2 - 1, 0f, 1f ) );

                    if ( reloadProgress > 0.999 )
                    {
                        reloadProgress = 0f;
                        changeState( State.Idle );
                    }
                }
                break;
            case State.Sticky:
                stickyProfress += Time.deltaTime / 0.5f;
                stickyProfress = Mathf.Clamp(stickyProfress, 0, 1);
                moveHand(ThrowPos.transform, stickyProfress);
                if ( Input.GetMouseButtonDown( ThrowButton ) )
                {
                    attachedDart = null;
                    cj.connectedBody = null;
                    lr.enabled = false;
                    stickyProfress = 0;
                    changeState(State.Reload);
                   
                }

                break;


            case State.Idle:
                if ( Input.GetMouseButton( ThrowButton ) && person.InControl)
                {

                    chargeProgress = 0f;
                    changeState( State.Charging );
                }
                break;
        }

        if ( sticky && attachedDart != null && ( transform.hasChanged || attachedDart.hasChanged ) )
        {
            lr.enabled = true;
            Vector3[ ] vec = { HandPos.position, attachedDart.position };
            lr.SetPositions( vec );
        }
        if ( sticky && attachedDart == null )
        {
            lr.enabled = false;
            cj.connectedBody = null;
        }
    }

    private void spawnDart( )
    {
        heldDart = Instantiate( DartPrefab, DartAnchor.transform );
        heldDart.transform.parent = Hand.transform;
        heldDart.transform.GetChild( 0 ).gameObject.layer = LayerMask.NameToLayer( "3D Screen Space" );
        if ( sticky )
        {
            heldDart.GetComponent< StickyDart >( ).dh = this;
        }
    }

    public void Attach( Vector3 worldpos, Collider col, Transform obj )
    {
        if ( curState != State.Sticky && curState != State.Throw)
        {
            return;
        }
        SoftJointLimit temp = cj.linearLimit;
        temp.limit = Vector3.Distance( cj.transform.position, worldpos );

        cj.connectedBody = col.attachedRigidbody;
      
        cj.connectedAnchor = col.transform.InverseTransformPoint( worldpos );
        cj.anchor = transform.InverseTransformPoint( HandPos.position );
        cj.linearLimit = temp;
        attachedDart = obj;
    }



    private void throwDart( )
    {

        Rigidbody dartRb = heldDart.GetComponent< Rigidbody >( );
        if ( dartRb == null )
        {
            dartRb = heldDart.AddComponent<Rigidbody>();
            dartRb.mass = 0.3f;
            dartRb.interpolation = RigidbodyInterpolation.Interpolate;
            dartRb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            heldDart.GetComponent<Collider>().enabled = true;
        }

        float throwStrength = Mathf.Lerp(MinThrowStrength, MaxThrowStrength, chargeProgress);
        dartRb.isKinematic = false;
        dartRb.AddForce(heldDart.transform.forward * throwStrength);

        heldDart.transform.parent = null;
        heldDart.gameObject.layer = LayerMask.NameToLayer( "Default" );
        foreach (Transform childTransform in heldDart.transform)
        {
            childTransform.gameObject.layer = LayerMask.NameToLayer("Default");
        }
        
        if ( sticky )
        {
            attachedDart = heldDart.transform;
            cj.connectedBody = null;
        }

        heldDart = null;
    }

    private void changeState( State newState )
    {
        curState = newState;
        updateStoredHandTransform( );
    }

    private void moveHand( Transform target, float progress )
    {
        Hand.transform.localPosition = Vector3.Lerp( storedHandPosOffset, target.localPosition, progress );
        Hand.transform.localRotation = Quaternion.Lerp( storedHandRotOffset, target.localRotation, progress );
    }

    private void updateStoredHandTransform( )
    {
        storedHandPosOffset = Hand.transform.localPosition;
        storedHandRotOffset = Hand.transform.localRotation;
    }
}
