﻿using UnityEngine;


public class FirstPerson : MonoBehaviour
{
    public Transform HeadMovement;
    public float Sens = 5;
    public float AccelForce = 50;
    public bool InControl = true;
    private float movespeedCap = 1.40f;



    private float yaw;
    private float pitch;
    private Quaternion startRotation;
    private Rigidbody rb;
    private float previous;
    private float MovespeedCapSqr;

    /// <summary>
    /// Called immediately after this <see cref="Component"/> instance is created.
    /// This is essentially a constructor for a <see cref="Component"/>, you cannot define your own constructor.
    /// </summary>
    private void Awake( )
    {
        startRotation = HeadMovement.localRotation;
        rb = GetComponentInChildren< Rigidbody >( );
        MovespeedCapSqr = ( Vector3.forward * movespeedCap ).sqrMagnitude;
    }


    void Start( )
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void FixedUpdate( )
    {
        if ( !InControl )
        {
            return;
        }

        var desiredDir = Vector3.zero;
        if ( Input.GetKey( KeyCode.D ) )
        {
            desiredDir += HeadMovement.right;
        }
        if ( Input.GetKey( KeyCode.A ) )
        {
            desiredDir += -HeadMovement.right;
        }
        if ( Input.GetKey( KeyCode.W ) )
        {
            desiredDir += HeadMovement.forward;
        }
        if ( Input.GetKey( KeyCode.S ) )
        {
            desiredDir += -HeadMovement.forward;
        }

        var direction = Vector3.ProjectOnPlane( desiredDir, Vector3.up ).normalized;

        rb.AddForce( direction * AccelForce);
        Debug.DrawRay( transform.position, direction );
        if ( rb.velocity.sqrMagnitude > MovespeedCapSqr )
        {
            rb.velocity = rb.velocity.normalized * movespeedCap;
        }
    }




    // Update is called once per frame
    void Update( )
    {

        yaw = yaw + Input.GetAxis( "Mouse X" ) * Sens;
        pitch = Mathf.Clamp( pitch - Input.GetAxis( "Mouse Y" ) * Sens, -89.9f, 89.9f );
        HeadMovement.localRotation = startRotation * Quaternion.AngleAxis( yaw, Vector3.up ) * Quaternion.AngleAxis( pitch, Vector3.right );
    }
}
