﻿using System.Collections.Generic;
using UnityEngine;
/*

public class DartSpawner : MonoBehaviour
{
    private float timeSinceLastDartThrown = 0.0f;
    public GameObject dartPrefab;
    public GameObject stickydart;
    public List< GameObject > pool;


    public float dartThrowPeriod = 1f;
    public int Limit = 100;




    [ HideInInspector ]
    public ConfigurableJoint cj;
    private LineRenderer lr;
    public GameObject currentDart;

    /// <summary>
    /// Called immediately after this <see cref="Component"/> instance is created.
    /// This is essentially a constructor for a <see cref="Component"/>, you cannot define your own constructor.
    /// </summary>
    private void Awake( )
    {
        cj = GetComponent< ConfigurableJoint >( );
        pool = new List< GameObject >( );
        lr = GetComponent< LineRenderer >( );
        lr.enabled = false;
    }


    // Use this for initialization
    void Start( ) { }


    public void Attach( Vector3 worldpos, Collider col )
    {
        SoftJointLimit temp = cj.linearLimit;
        temp.limit = Vector3.Distance( cj.transform.position, worldpos );
        cj.connectedBody = col.GetComponent< Rigidbody >( );
        cj.connectedAnchor = col.transform.InverseTransformPoint( worldpos );
        cj.linearLimit = temp;




    }

    void updatepool( GameObject dart )
    {
        pool.Add( dart );
        if ( pool.Count > Limit )
        {
            var obj = pool[ 0 ];
            pool.RemoveAt( 0 );
            Destroy( obj );
        }
    }

    // Update is called once per frame
    void Update( )
    {
        timeSinceLastDartThrown += Time.deltaTime;

        if ( timeSinceLastDartThrown > dartThrowPeriod )
        {
            if ( Input.GetMouseButton( 0 ) )
            {

                GameObject dart = Instantiate( dartPrefab, transform.position, transform.rotation );

                dart.GetComponent< Rigidbody >( ).AddForce( transform.forward * 500 );

                timeSinceLastDartThrown = 0.0f;
            }
            else if ( Input.GetMouseButton( 1 ) )
            {
                GameObject dart = Instantiate( stickydart, transform.position, transform.rotation );

                dart.GetComponent< Rigidbody >( ).AddForce( transform.forward * 500 );
                dart.GetComponent< StickyDart >( ).dh = this;
                cj.connectedBody = null;
                currentDart = dart;
                timeSinceLastDartThrown = 0.0f;
                updatepool( dart );
            }
        }

        if ( currentDart )
        {
            if ( currentDart.transform.hasChanged || transform.hasChanged )
            {
                lr.enabled = true;
                lr.SetPosition( 0, transform.position );
                lr.SetPosition( 1, currentDart.transform.position );


            }
        }


    }
}
*/