﻿using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class Rat : MonoBehaviour
{
    public float speed = 1f;
    public GameObject ratModel;

    private float walkCycleProgress = 0f;
    private bool isDead = false;

    // Use this for initialization
    void Start () {
        
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.transform.tag == "Dart")
        {
            isDead = true;
            Objectives.Singleton.KilledRat();


            gameObject.AddComponent<Rigidbody>();



            var sticky = col.transform.GetComponent<StickyDart>();
            if (sticky != null)
            {
                sticky.dh.Attach(sticky.stickpoint.position, GetComponent<Collider>(), sticky.transform);
            }

        }
    }


    // Update is called once per frame
    void Update () {
        if (!isDead)
        {
            transform.RotateAround(transform.position, transform.forward, Time.deltaTime * 30);
            transform.position += -transform.right * speed * Time.deltaTime;

            ratModel.transform.localRotation = Quaternion.Lerp(Quaternion.Euler(new Vector3(0, 5, 0)), Quaternion.Euler(new Vector3(0, -5, 0)), Mathf.Sin(walkCycleProgress));
            walkCycleProgress += Time.deltaTime * 50;
        }
	}
}
