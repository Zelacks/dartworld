﻿using System.Collections.Generic;
using UnityEngine;


public class Laptop : MonoBehaviour
{
    public Material off;
    public Material budMessage;
    public Material budResponse;
    public Material gfMessage;
    public Material gfResponse;

    public Transform board;

    private Dictionary<Light, float> lightsandstartintensity;
    public Light keyLight;
    private bool started = false;
    public float fade = 5.0f;

    private float time = 0.0f;
    private State curState;

    enum State
    {
        Off,
        BudMessaged,
        BudRespondedTo,
        GfMessages,
        GfRespondedTo,
        Done
    }

    /// <summary>
    /// Called immediately after this <see cref="Component"/> instance is created.
    /// This is essentially a constructor for a <see cref="Component"/>, you cannot define your own constructor.
    /// </summary>
    private void Awake()
    {
        keyLight.enabled = false;
        board.gameObject.SetActive(false);
    }

    // Use this for initialization
    void Start()
    {
        changeScreen(off);
    }

    // Update is called once per frame
    void Update()
    {
        if (curState == State.Done)
        {
            if (!started)
            {
                board.gameObject.SetActive(true);
                lightsandstartintensity = new Dictionary<Light, float>();
                var lights = FindObjectsOfType<Light>();
                foreach (var curlight in lights)
                {
                    lightsandstartintensity[curlight] = curlight.intensity;
                }
                keyLight.enabled = true;
                started = true;
            }

            time += Time.deltaTime;

            foreach (var pair in lightsandstartintensity)
            {
                pair.Key.intensity = Mathf.Lerp(pair.Value, 0.0f, time / fade);
            }
            keyLight.intensity = Mathf.Lerp(0.0f, 1.0f, time / fade);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.transform.tag == "Dart")
        {
            switch (curState)
            {
                case State.Off:
                    if (Objectives.Singleton.State == Objectives.ObjectiveState.CheckFriend)
                    {
                        changeState(State.BudMessaged);
                    }
                    else if (Objectives.Singleton.State == Objectives.ObjectiveState.CheckGF)
                    {
                        changeState(State.GfMessages);
                    }
                    break;
                case State.BudMessaged:
                    if (Objectives.Singleton.State == Objectives.ObjectiveState.CheckFriend)
                    {
                        Objectives.Singleton.ChatBuddo();
                        changeState(State.BudRespondedTo);
                    }
                    break;
                case State.BudRespondedTo:
                    if (Objectives.Singleton.State == Objectives.ObjectiveState.CheckGF)
                    {
                        changeState(State.GfMessages);
                    }
                    else
                    {
                        changeState(State.Off);
                    }
                    break;
                case State.GfMessages:
                    changeState(State.GfRespondedTo);
                    break;
                case State.GfRespondedTo:
                    Objectives.Singleton.QuestionMark();
                    changeState(State.Done);
                    break;
                case State.Done:
                    break;
            }
        }
    }

    private void changeState(State newState)
    {
        switch (newState)
        {
            case State.Off:
                changeScreen(off);
                break;
            case State.BudMessaged:
                changeScreen(budMessage);
                break;
            case State.BudRespondedTo:
                changeScreen(budResponse);
                break;
            case State.GfMessages:
                changeScreen(gfMessage);
                break;
            case State.GfRespondedTo:
                changeScreen(gfResponse);
                break;
            case State.Done:
                changeScreen(off);
                break;
        }

        curState = newState;
    }

    private void changeScreen(Material newScreenMat)
    {
        Material[] mats = GetComponent<MeshRenderer>().materials;
        mats[2] = newScreenMat;
        GetComponent<MeshRenderer>().materials = mats;
    }
}