﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class dart : MonoBehaviour
{
    private Rigidbody rb;
    


    void Start()
    {
        
    }

    void Update()
    {
        var rb = GetComponent<Rigidbody>();
        if (rb != null && rb.isKinematic == false)
        {
            transform.forward = Vector3.Slerp(transform.forward, rb.velocity.normalized, Time.deltaTime * 10);
        }
    }

    void OnCollisionEnter(Collision col)
    {
        var rb = GetComponent<Rigidbody>();
        if (rb != null && rb.isKinematic == false && (col.gameObject.tag != "Player" && col.gameObject.tag != "Dart"))
        {
            AudioSource.PlayClipAtPoint(Assets.Singleton.DartHits.OrderBy(qu => Guid.NewGuid()).First(), transform.position);

            transform.parent = col.collider.transform;
            Destroy(GetComponent<BoxCollider>());
            Destroy(GetComponent<Rigidbody>());
        }
    }
}
