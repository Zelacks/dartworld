﻿using System;
using System.Linq;
using UnityEngine;

public class StickyDart : MonoBehaviour
{
    public Transform stickpoint;
    private Rigidbody rb;

    [HideInInspector]
    public DartHand dh;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        rb = GetComponent<Rigidbody>();
        if ( rb != null && rb.isKinematic == false )
        {
            transform.forward = Vector3.Slerp( transform.forward, rb.velocity.normalized, Time.deltaTime * 10 );
        }
    }

    void OnCollisionEnter(Collision col)
    {
        rb = GetComponent<Rigidbody>();
        if (rb != null && rb.isKinematic == false && (col.gameObject.tag != "Player" && col.gameObject.tag != "Dart"))
        {
            AudioSource.PlayClipAtPoint(Assets.Singleton.DartHits.OrderBy(qu => Guid.NewGuid()).First(), transform.position );

            GetComponent< Rigidbody >( ).isKinematic = true;
            transform.parent = col.collider.transform;
            transform.position = col.contacts[ 0 ].point;
            dh.Attach( stickpoint.position, col.collider, this.transform );
            Destroy( GetComponent< Collider >( ) );
            Destroy( GetComponent< Rigidbody >( ) );
        }
    }
}
