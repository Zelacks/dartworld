﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Assets : MonoBehaviour
{
    public AudioClip[] DartHits;
    public AudioClip[] BoardResponses;
    public AudioClip[] Bullseye;
    public AudioClip Eat;

    private static Assets singleton;

    static public Assets Singleton
    {
        get
        {
            if (singleton == null)
            {
                singleton = FindObjectOfType<Assets>();
            }
            return singleton;
        }
    }
}